let arrColor = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermilion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
// Cách 1:
// let renderButton = () => {
//   for (let i = 0; i < arrrColor.length; i++) {
//     // Mỗi lần duyệt tạo ra một giá trị màu
//     let color = arrrColor[i];
//     // Từ giá trị màu tạo ra button
//     let btn = document.createElement("button");
//     btn.className = "btn text-white ml-2";
//     btn.innerHTML = color;
//     btn.style.backgroundColor = color;
//     btn.onclick = function () {
//       // Dom đến div home => change style color
//       document.querySelector("#home").style.color = color;
//     };
//     // Hiện thị button qua giao diện
//     document.querySelector("#color").appendChild(btn);
//   }
// };
// renderButton();

// Cách 2:
let renderButton = () => {
  let contentHTML = "";
  for (let i = 0; i < arrColor.length; i++) {
    let color = arrColor[i];
    let contentBtn = ` <button class="color-button ${color}" onclick="changeColor('${color}')">
    </button>`;
    contentHTML = contentHTML + contentBtn;
  }
  document.querySelector("#colorContainer").innerHTML = contentHTML;
};
renderButton();

let changeColor = (color) => {
  document.querySelector("#house").className = "house " + color;
};
