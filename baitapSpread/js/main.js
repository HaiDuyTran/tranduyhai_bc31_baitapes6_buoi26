let text = document.getElementById("heading").innerHTML;
let chars = [...text];
let contentHTML = "";
for (let i = 0; i < chars.length; i++) {
  let alphabet = chars[i];
  let contentItem = ` <span>${alphabet}</span>`;
  contentHTML = contentHTML + contentItem;
}
document.getElementById("heading").innerHTML = contentHTML;
