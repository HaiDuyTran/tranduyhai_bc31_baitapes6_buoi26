let tinhDTB = (...theArgs) => {
  let total = 0;
  for (const arg of theArgs) {
    total += arg;
  }
  return total / theArgs.length;
};

let tinhTbKhoi1 = () => {
  let diemToan = document.getElementById("inpToan").value * 1;
  let diemLy = document.getElementById("inpLy").value * 1;
  let diemHoa = document.getElementById("inpHoa").value * 1;
  console.log(diemToan, diemLy, diemHoa);
  let diemTB = tinhDTB(diemToan, diemLy, diemHoa);
  document.getElementById("tbKhoi1").innerHTML = diemTB;
};
let tinhTbKhoi2 = () => {
  let diemVan = document.getElementById("inpVan").value * 1;
  let diemSu = document.getElementById("inpSu").value * 1;
  let diemDia = document.getElementById("inpDia").value * 1;
  let diemEnglish = document.getElementById("inpEnglish").value * 1;
  let diemTB = tinhDTB(diemVan, diemSu, diemDia, diemEnglish);
  document.getElementById("tbKhoi2").innerHTML = diemTB;
};
