let diem = [1, 2, 3, 4, 5, 6, 7];

function myDiem(...diem) {
  let count = 0;
  let sum = 0;
  for (let i = 0; i < diem.length; i++) {
    sum += diem[i];
    count++;
  }
  return sum / count;
}
let DTB = myDiem(diem);
console.log("DTB: ", DTB);
